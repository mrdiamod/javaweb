package by.epam.model.beans;

/**
 * Book entity.
 */
public class Book {

    /**
     * Book's id.
     * Book's name
     * Book's author
     * Book's description
     * Book's year
     * Book's price
     */
    private int id;
    private String name;
    private String author;
    private String description;
    private String year;
    private int price;

    /**
     * Constructor without parameters.
     */
    public Book() {
        this(0, null, null, null, null, 0);
    }

    /**
     * Constructor with parameters.
     *
     * @param id     Book id
     * @param name   Book's name
     * @param author Book's author
     * @param description Book's description
     * @param year   Book's year of publishing
     * @param price  Book's prices
     */
    public Book(int id, String name, String author, String description, String year, int price) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.description = description;
        this.year = year;
        this.price = price;
    }

    public Book(String name, String author, String description, String year, int price) {
        this.name = name;
        this.author = author;
        this.description = description;
        this.year = year;
        this.price = price;
    }


    public Book(int id, String name) {
        this.id = id;
        this.name = name;
        this.author = null;
        this.description = null;
        this.year = null;
        this.price = 0;
    }

    /**
     * Getters and setters
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Override hashCode method
     *
     * @return hashCode
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /**
     * Override equals method
     *
     * @param obj object
     * @return boolean equals
     */
    @Override
    public boolean equals(Object obj) {
        return obj == this;
    }

    /**
     * Override method toString
     *
     * @return Book in string format
     */
    @Override
    public String toString() {
        return id + ";" + name + ";" + author + ";" + description + ";" + year + ";" + price;
    }
}
