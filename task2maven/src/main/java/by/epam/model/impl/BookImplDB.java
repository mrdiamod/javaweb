package by.epam.model.impl;

import by.epam.controller.utils.ConstantsSQL;
import by.epam.controllers.exceptions.DAOException;
import by.epam.interfaces.IBookDAO;
import by.epam.model.beans.Book;
import by.epam.model.utils.MySqlConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * DAO db from name "epam lib".
 */
public class BookImplDB implements IBookDAO {
    private static final Logger LOGGER = Logger.getLogger(BookImplDB.class);
    private static final String SQL_SELECT_BOOK_ALL = "SELECT id,name FROM books";
    private static final String SQL_SELECT_ADD_BOOK =
            "INSERT INTO books (`name`, `author`, `description`, `year`, `price`) " +
                    "VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_SELECT_BOOK_BY_ID = "SELECT * FROM books WHERE id= ?";
    private static final String SQL_UPDATE_BOOK_BY_ID = "UPDATE books " +
            "SET name = ?, author = ?, description = ?, year = ?, price = ? WHERE id = ?";
    private static final String SQL_DELETE_BOOK_BY_ID = "DELETE FROM books WHERE id= ?";
    /**
     * Get all books from epamlib.
     *
     * @return List of books
     * @throws DAOException SQL exception
     */
    @Override
    public List<Book> getAllBooks() throws DAOException {
        List<Book> list = null;
        try (Connection conn = MySqlConnectionPool.getConnection();
             PreparedStatement prSt = conn.prepareStatement(SQL_SELECT_BOOK_ALL);
             ResultSet rs = prSt.executeQuery()) {
            list = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt(ConstantsSQL.ID);
                String name = rs.getString(ConstantsSQL.NAME);
                list.add(new Book(id, name));
            }
        } catch (SQLException e) {
            LOGGER.error(Arrays.toString(e.getStackTrace()) + ";" + e.getMessage());
            throw new DAOException(e);
        }
        LOGGER.info(list);
        return list;
    }

    /**
     * public void method when add book in epamlib.
     *
     * @param paramBookName     field
     * @param paramBookAuthor   field
     * @param paramBookDescript field
     * @param paramBookDate     field
     * @param paramBookPrice    field
     * @return not return, operation add book in List of books
     * @throws DAOException SQL exception
     */
    @Override
    public void addBook(String paramBookName, String paramBookAuthor,
                        String paramBookDescript, String paramBookDate, String paramBookPrice) throws DAOException {
        Book addBook = new Book(paramBookName, paramBookAuthor,
                paramBookDescript, paramBookDate, Integer.valueOf(paramBookPrice));
        try (Connection conn = MySqlConnectionPool.getConnection();
             PreparedStatement prSt = addArgsInSqlQuery(conn.prepareStatement(SQL_SELECT_ADD_BOOK), addBook)) {
            prSt.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(Arrays.toString(e.getStackTrace()) + ";" + e.getMessage());
            throw new DAOException(e);
        }
    }

    /**
     * public method with Get book from epamlib.
     *
     * @param id seacrh book by id in DB
     * @return Book
     * @throws DAOException SQL exception
     */
    @Override
    public Book getBook(int id) throws DAOException {
        Book book = null;
        try (Connection conn = MySqlConnectionPool.getConnection();
             PreparedStatement prSt = conn.prepareStatement(SQL_SELECT_BOOK_BY_ID)) {
            prSt.setInt(1, id);
            ResultSet rs = prSt.executeQuery();
            while (rs.next()) {
                String nameB = rs.getString(ConstantsSQL.NAME);
                String authorB = rs.getString(ConstantsSQL.AUTHOR);
                String descripB = rs.getString(ConstantsSQL.DESCRIPTION);
                String yearB = rs.getString(ConstantsSQL.YEAR);
                int priceB = rs.getInt(ConstantsSQL.PRICE);
                book = new Book(id, nameB, authorB, descripB, yearB, priceB);
            }
        } catch (SQLException e) {
            LOGGER.error(Arrays.toString(e.getStackTrace()) + ";" + e.getMessage());
            throw new DAOException(e);
        }
        return book;
    }


    /**
     * public void method when edit book from epamlib.
     *
     * @param paramBookId       1 field
     * @param paramBookName     2 field
     * @param paramBookAuthor   3 field
     * @param paramBookDescript 4 field
     * @param paramBookDate     5 field
     * @param paramBookPrice    6 field
     * @return not return, operation edit book in List of books
     * @throws DAOException SQL exception
     */
    @Override
    public void editBook(String paramBookId, String paramBookName, String paramBookAuthor,
                         String paramBookDescript, String paramBookDate, String paramBookPrice) throws DAOException {
        Book editBook = new Book(Integer.parseInt(paramBookId), paramBookName, paramBookAuthor,
                paramBookDescript, paramBookDate, Integer.parseInt(paramBookPrice));
        try (Connection conn = MySqlConnectionPool.getConnection();
             PreparedStatement prSt = addArgsInSqlQuery(conn.prepareStatement(SQL_UPDATE_BOOK_BY_ID), editBook)) {
            prSt.setInt(6, editBook.getId());
            prSt.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(Arrays.toString(e.getStackTrace()) + ";" + e.getMessage());
            throw new DAOException(e);
        }
    }

    /**
     * public method with delete book from epamlib.
     *
     * @param id delete book by id in DB
     * @throws DAOException SQL exception
     */
    @Override
    public void deleteBook(int id) throws DAOException {
        try (Connection conn = MySqlConnectionPool.getConnection();
             PreparedStatement prSt = conn.prepareStatement(SQL_DELETE_BOOK_BY_ID)) {
            prSt.setInt(1, id);
            prSt.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(Arrays.toString(e.getStackTrace()) + ";" + e.getMessage());
            throw new DAOException(e);
        }
    }

    private PreparedStatement addArgsInSqlQuery(PreparedStatement prSt, Book book) throws DAOException {
        try {
            prSt.setString(1, book.getName());
            prSt.setString(2, book.getAuthor());
            prSt.setString(3, book.getDescription());
            prSt.setString(4, book.getYear());
            prSt.setInt(5, book.getPrice());
        } catch (SQLException e) {
            LOGGER.error(Arrays.toString(e.getStackTrace()) + ";" + e.getMessage());
            throw new DAOException(e);
        }
        return prSt;
    }

}
