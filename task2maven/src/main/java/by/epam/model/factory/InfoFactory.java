package by.epam.model.factory;

import by.epam.controller.utils.Constants;
import by.epam.interfaces.IBookDAO;
import by.epam.model.impl.BookImplDB;

/**
 * InfoFactory set where type connections of web.xml
 * and data source
 */
public class InfoFactory {
    private static IBookDAO bookDAO;

    private InfoFactory(){
    }
    /**
     * method setGlobals
     * 
     * @param strDAO string, where parameter is type DataSource
     * implementation is type DataSource
     */
    public static void setGlobals(String strDAO) {
        if (Constants.KEY_DB.equals(strDAO)) {
            bookDAO = new BookImplDB();
        }
    }

    /**
     * @return bookDAO, type data source
     */
    public static IBookDAO getClassFromFactory() {
        return bookDAO;
    }
}