package by.epam.model.utils;

public class Fibonacci {
    private Fibonacci(){
    }

    public static long calculatePosition(int position) {
        long n0 = 0;
        long n1 = 1;
        long n2;
        if (position == 0)
            return n0;
        for (int i = 2; i <= position; i++) {
            n2 = n0 + n1;
            n0 = n1;
            n1 = n2;
        }
        return n1;
    }
}
