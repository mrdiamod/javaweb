package by.epam.model.utils;

import by.epam.controllers.exceptions.DAOException;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * ConnectionPool open
 * gives a connection to the database
 */
public class MySqlConnectionPool {
    private static final Logger LOGGER = Logger.getLogger(MySqlConnectionPool.class);
    private static HikariConfig config = new HikariConfig("/datasource.properties");
    private static HikariDataSource hikariDataSource;

    static {
        hikariDataSource = new HikariDataSource(config);
    }

    private MySqlConnectionPool() {
    }

    /**
     * give get connection with db
     *
     * @return opens connection
     * @throws DAOException SQL exception
     */
    public static Connection getConnection() throws DAOException, SQLException {
        LOGGER.info("getConnection");
        return hikariDataSource.getConnection();
    }

}
