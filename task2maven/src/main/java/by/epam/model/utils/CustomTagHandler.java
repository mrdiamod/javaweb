package by.epam.model.utils;

import org.apache.log4j.Logger;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.*;

public class CustomTagHandler extends TagSupport {
    private static final Logger LOGGER = Logger.getLogger(CustomTagHandler.class);
    private Queue<Map.Entry<String, ? extends ServletRegistration>> queue;

    @Override
    public int doStartTag() {
        JspWriter out = pageContext.getOut();
        try {
            out.print(Calendar.getInstance().getTime());
        } catch (IOException e) {
            LOGGER.error(e.getStackTrace());
        }
        queue = new ArrayDeque<>();
        Map<String, ? extends ServletRegistration> servletRegistrations =
                pageContext.getServletContext().getServletRegistrations();
        getNameAndMapppingsServlets(servletRegistrations);
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() {
        if (!queue.isEmpty()) {
            Map.Entry<String, ? extends ServletRegistration> mapEntry = queue.poll();
            Iterator<String> iteratorUrls = mapEntry.getValue().getMappings().iterator();
            pageContext.setAttribute("nameServlet", mapEntry.getKey());
            if(iteratorUrls.hasNext()){
                pageContext.setAttribute("mapServlet", iteratorUrls.next());
            }
            return EVAL_BODY_AGAIN;
        }
        int currentSecond = Calendar.getInstance().get(Calendar.SECOND);
        fibonacciBySeconds(currentSecond);
        return SKIP_BODY;
    }

    private void fibonacciBySeconds(int currentSecond) {
        JspWriter out = pageContext.getOut();
        String result = "Fibonacci by Second:";
            result += Fibonacci.calculatePosition(currentSecond);
        try {
            out.print(result);
        } catch (IOException e) {
            LOGGER.error(e.getStackTrace());
        }
    }

    private void getNameAndMapppingsServlets(Map<String, ? extends ServletRegistration> servletRegistrations) {
        for (Map.Entry<String, ? extends ServletRegistration> entry : servletRegistrations.entrySet()) {
            queue.add(entry);
        }
    }
}
