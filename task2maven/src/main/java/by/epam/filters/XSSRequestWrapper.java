package by.epam.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.regex.Pattern;

public class XSSRequestWrapper extends HttpServletRequestWrapper {

    public XSSRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    private static Pattern[] patterns = new Pattern[]{
            // Script fragments
            Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE),
            // src='...'
            Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
            Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
            // lonely script tags
            Pattern.compile("</script>", Pattern.CASE_INSENSITIVE),
            Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
            // javascript:...
            Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE),
            // onload(...)=...
            Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL)
    };

    /**
     * The default behavior of this method is to return
     * getParameterValues(String name) on the wrapped request object.
     *
     * @param value String
     */
    @SuppressWarnings(
            {"if values == null when return value ,ConstantConditions ©Jenkins"})
    @Override
    public String[] getParameterValues(String value) {
        String[] values = super.getParameterValues(value);
        if (values == null) {
            return values;
        }
        int length = values.length;
        String[] encodedValues = new String[length];
        for (int i = 0; i < length; i++) {
            encodedValues[i] = processingStripXSS(values[i]);
        }
        return encodedValues;
    }

    /**
     * The default behavior of this method is to return
     * getParameter(String name) on the wrapped request object.
     *
     * @param param String
     */
    @Override
    public String getParameter(String param) {
        String value = super.getParameter(param);
        return processingStripXSS(value);
    }

    private String processingStripXSS(String value) {
        if (value != null) {
            value = value
                    .replaceAll("\0", "")
                    .replaceAll(" ", "")
                    .replaceAll("script", "")
                    .replaceAll("<", "&lt;")
                    .replaceAll(">", "&gt;");

            // Remove all sections that match a pattern
            for (Pattern scriptPattern : patterns) {
                value =
                        scriptPattern
                                .matcher(value)
                                .replaceAll("");
            }
        }
        return value;
    }
}
