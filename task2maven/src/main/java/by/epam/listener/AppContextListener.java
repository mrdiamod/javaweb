package by.epam.listener;

import by.epam.controller.utils.Constants;
import by.epam.model.factory.InfoFactory;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import java.util.Iterator;
import java.util.Map;

/**
 * Listener for servlet context.
 */
@WebListener
public class AppContextListener implements ServletContextListener {
    private static final Logger LOG = Logger.getLogger(AppContextListener.class);

    /**
     * fixed all action servlet context to console
     *
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String strDAO = sce.getServletContext().getInitParameter(Constants.KEY_DAO);
        InfoFactory.setGlobals(strDAO);
        LOG.info("Server info: " + sce.getServletContext().getServerInfo());
        LOG.info("App name: " + sce.getServletContext().getServletContextName());
        LOG.info("Context param: " + strDAO);
        Map<String, ? extends ServletRegistration> maps = sce.getServletContext().getServletRegistrations();
        LOG.info("Available Servlets: " + String.join("; ", maps.keySet()));
        for (Map.Entry<String, ? extends ServletRegistration> entry : maps.entrySet()) {
            LOG.info("Available URL: " + entry.getValue().getMappings().iterator().next());
        }
    }

    /**
     * fixed when Application is close
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOG.info("Application close");
    }
}
