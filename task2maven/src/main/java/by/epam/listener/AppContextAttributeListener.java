package by.epam.listener;

import org.apache.log4j.Logger;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

/**
 * Listener for servlet context attribute.
 */
@WebListener
public class AppContextAttributeListener implements ServletContextAttributeListener {
    private static final Logger LOG = Logger.getLogger(AppContextAttributeListener.class);

    /**
     * fixed action if attrbute added in console
     *
     * @param event
     */
    @Override
    public void attributeAdded(ServletContextAttributeEvent event) {
        LOG.info("Serv.Cont. attr. added::{" + event.getName() + "," + event.getValue() + "}");
    }

    /**
     * fixed action if attrbute removed in console
     *
     * @param event
     */
    @Override
    public void attributeRemoved(ServletContextAttributeEvent event) {
        LOG.info("Serv.Cont attr repl.::{" + event.getName() + "," + event.getValue() + "}");
    }

    /**
     * fixed action if attrbute replaced in console
     *
     * @param event
     */
    @Override
    public void attributeReplaced(ServletContextAttributeEvent event) {
        LOG.info("Serv.Cont. attr. remov.::{" + event.getName() + "," + event.getValue() + "}");
    }
}
