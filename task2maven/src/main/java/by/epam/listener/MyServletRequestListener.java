package by.epam.listener;

import org.apache.log4j.Logger;

import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

/**
 * Listener for servlet request.
 */
@WebListener
public class MyServletRequestListener implements ServletRequestListener {
    private static final Logger LOG = Logger.getLogger(MyServletRequestListener.class);

    /**
     * fixed action if request destroyed in console
     *
     * @param sre
     */
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        ServletRequest servletRequest = sre.getServletRequest();
        LOG.info("ServlReq destr.. Remote IP=" + servletRequest.getRemoteAddr());
    }

    /**
     * fixed action if request initialized in console
     *
     * @param sre
     */
    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        ServletRequest servletRequest = sre.getServletRequest();
        LOG.info("ServlReq init.. Remote IP=" + servletRequest.getRemoteAddr());
    }
}
