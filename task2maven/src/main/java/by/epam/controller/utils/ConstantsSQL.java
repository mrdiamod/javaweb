package by.epam.controller.utils;

/**
 * This is all Constants column sql
 */
public class ConstantsSQL {
    private ConstantsSQL() {
    }

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String AUTHOR = "author";
    public static final String DESCRIPTION = "description";
    public static final String YEAR = "year";
    public static final String PRICE = "price";
}
