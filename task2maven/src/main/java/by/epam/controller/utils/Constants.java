package by.epam.controller.utils;

/**
 * This is all Constants
 */
public class Constants {
    private Constants() {
    }

    //page
    public static final String ERROR_PAGE = "/errorPage.jsp";
    public static final String START_PAGE = "/start.jsp";
    public static final String OPERATION_LIST_BOOKS_PAGE = "/operation/listBooks.jsp";
    public static final String EDIT_PAGE = "/operation/editBook.jsp";
    //
    public static final String EMPTY = "";
    public static final String KEY_ERROR_MESSAGE = "errorMessage";
    public static final String ERROR_NULL = "Login or password is missing.";
    public static final String ERROR_EMPTY = "Login or password is empty.";
    public static final String ERROR_PASS = "Wrong password.";
    public static final String ERROR_LOGIN = "Wrong login";
    public static final String ERROR_SOURCE = "Input source proccessing problems.";
    public static final String ERROR_LOGIN_EXISTS = "This login already exists";

    //sql, connection, DB, data base
    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_NAME = "epam";
    public static final String DB_URL = "jdbc:mysql://localhost:3306/" +DB_NAME + "?useUnicode=true&serverTimezone=UTC";
    public static final String REGIONAL_PROPERTIES = "Europe/Minsk;characterEncoding=utf8";
    public static final String DB_LOGIN = "root";
    public static final String DB_PASS = "admin";

    //
    public static final String KEY_DAO = "dao";
    public static final String KEY_HARCODED = "hardcoded";
    public static final String KEY_DB = "db";
    public static final String KEY_BOOKS = "books";
    public static final String KEY_EDIT_BOOK = "editBook";
    public static final int MAX_TOTAL = 10;
    public static final int MIN_IDLE = 3;
    public static final int MAX_IDLE = 7;
    public static final int MAX_OPEN_STATEMENTS = 9;

    public static final String ERROR_OUUPS = "ouuuuppps";
    public static final String PARAM_FLAG_ERROR = "flagError";

    //for LOGGER
    public static final String INIT = "init";
    public static final String DO_GET = "doGet";
    public static final String DO_POST = "doPost";
    public static final String DO_DELETE = "doDelete";

}
