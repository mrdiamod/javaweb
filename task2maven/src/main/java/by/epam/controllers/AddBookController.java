package by.epam.controllers;

import by.epam.controller.utils.Constants;
import by.epam.controllers.exceptions.DAOException;
import by.epam.interfaces.IBookDAO;
import by.epam.model.factory.InfoFactory;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Servlet for Add Book in DB.
 *
 * method doPost for add Book in DB and
 * print information Books in page
 *
 * <p>
 * extends abstract base controller
 */

public class AddBookController extends AbstractBaseController {
    private static final Logger LOGGER = Logger.getLogger(AddBookController.class);

    /**
     * Called by the server (via the <code>service</code> method)
     * to allow a servlet to handle a POST request.
     * <p>
     * The HTTP POST method allows the client to send
     * data of unlimited length to the Web server a single time
     * and is useful when posting information such as
     * credit card numbers.
     *
     * @param req  an {@link HttpServletRequest} object that
     *             contains the request the client has made
     *             of the servlet
     * @param resp an {@link HttpServletResponse} object that
     *             contains the response the servlet sends
     *             to the client
     * @throws IOException      if an input or output error is
     *                          detected when the servlet handles
     *                          the request
     * @throws ServletException if the request for the POST
     *                          could not be handled
     * @see ServletOutputStream
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info(Constants.DO_POST);

        String categoryParam = req.getParameter("eventClickAddBook");

        String paramBookName = req.getParameter("addBookName");
        String paramBookAuthor = req.getParameter("addBookAuthor");
        String paramBookDescript = req.getParameter("addBookDescription");
        String paramBookDate = req.getParameter("addBookDate");
        String paramBookPrice = req.getParameter("addBookPrice");

        IBookDAO bookDAO = InfoFactory.getClassFromFactory();
        if (categoryParam.equals("add")) {
            try {
                bookDAO.addBook(paramBookName, paramBookAuthor,
                        paramBookDescript, paramBookDate, paramBookPrice);
            } catch (DAOException e) {
                LOGGER.error(e.getStackTrace());
                forwardError(e.getMessage(), Constants.ERROR_PAGE, req, resp);
            }
            resp.sendRedirect(req.getContextPath() + "/start");
        }

    }
}
