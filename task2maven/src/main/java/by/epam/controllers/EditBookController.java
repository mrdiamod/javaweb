package by.epam.controllers;

import by.epam.controller.utils.Constants;
import by.epam.controllers.exceptions.DAOException;
import by.epam.interfaces.IBookDAO;
import by.epam.model.beans.Book;
import by.epam.model.factory.InfoFactory;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet for Edit Book.
 *  method doGet for print book in page and edit
 *
 *  method doPost for update information in DB
 *  of this Book and redirect in back page
 *
 * <p>
 * extends abstract base controller
 */
public class EditBookController extends AbstractBaseController {
    private static final Logger LOGGER = Logger.getLogger(EditBookController.class);

    /**
     * Override 'doGet' method. Print Edit Book from database by id to Edit.jsp.
     *
     * @param req  request from browser
     * @param resp response from servlet
     * @throws ServletException Servlet exception
     * @throws IOException      IO exception
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info(Constants.DO_GET);
        int paramIdBook = Integer.parseInt(req.getParameter("id"));
        IBookDAO bookDAO = InfoFactory.getClassFromFactory();
        Book bookForEdit = null;
        try {
            bookForEdit = bookDAO.getBook(paramIdBook);
        } catch (DAOException e) {
            LOGGER.error(e.getStackTrace());
            forwardError(e.getMessage(), Constants.ERROR_PAGE, req, resp);
        }
        req.setAttribute(Constants.KEY_EDIT_BOOK, bookForEdit);
        forward(Constants.EDIT_PAGE, req, resp);
    }

    /**
     * Called by the server (via the <code>service</code> method)
     * to allow a servlet to handle a POST request.
     * <p>
     * The HTTP POST method allows the client to send
     * data of unlimited length to the Web server a single time
     * and is useful when posting information such as
     * credit card numbers.
     *
     * @param req  an {@link HttpServletRequest} object that
     *             contains the request the client has made
     *             of the servlet
     * @param resp an {@link HttpServletResponse} object that
     *             contains the response the servlet sends
     *             to the client
     * @throws IOException      if an input or output error is
     *                          detected when the servlet handles
     *                          the request
     * @throws ServletException if the request for the POST
     *                          could not be handled
     * @see ServletOutputStream
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info(Constants.DO_POST);
        String paramBookId = req.getParameter("editBookId");
        String paramBookName = req.getParameter("editBookName");
        String paramBookAuthor = req.getParameter("editBookAuthor");
        String paramBookDescript = req.getParameter("editBookDescription");
        String paramBookDate = req.getParameter("editBookDate");
        String paramBookPrice = req.getParameter("editBookPrice");
        IBookDAO bookDAO = InfoFactory.getClassFromFactory();

        try {
            bookDAO.editBook(paramBookId, paramBookName, paramBookAuthor,
                    paramBookDescript, paramBookDate, paramBookPrice);
        } catch (DAOException e) {
            LOGGER.error(e.getStackTrace());
            forwardError(e.getMessage(), Constants.ERROR_PAGE, req, resp);
        }
        resp.sendRedirect(req.getContextPath() + "/start");
    }


}
