package by.epam.controllers;

import by.epam.controller.utils.Constants;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class AbstractBaseController extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(AbstractBaseController.class);

    /**
     * Method forwards confirm to url
     *
     * @param url      - url
     * @param request  - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException      - an input or output operation is failed or interpreted
     */
    protected void forward(String url, HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(url);
        RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
        rd.forward(request, response);
    }

    /**
     * Method forwards error message confirm to url
     *
     * @param url      - url
     * @param message  - message
     * @param request  - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException      - an input or output operation is failed or interpreted
     */
    protected void forwardError(String message, String url, HttpServletRequest request,
                                HttpServletResponse response) throws ServletException, IOException {
        LOGGER.error(message);
        // to put a message into the request scope
        request.setAttribute(Constants.KEY_ERROR_MESSAGE, message);
        forward(url, request, response);
    }
}
