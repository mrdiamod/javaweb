package by.epam.controllers;

import by.epam.controller.utils.Constants;
import by.epam.controllers.exceptions.DAOException;
import by.epam.interfaces.IBookDAO;
import by.epam.model.factory.InfoFactory;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteBookController extends AbstractBaseController {
    private static final Logger LOGGER = Logger.getLogger(DeleteBookController.class);


    /**
     * Delete book by id.
     *
     * Called by the server (via the <code>service</code> method)
     * to allow a servlet to handle a DELETE request.
     * <p>
     * The DELETE operation allows a client to remove a document
     * or Web page from the server.
     *
     * @param req  the {@link HttpServletRequest} object that
     *             contains the request the client made of
     *             the servlet
     * @param resp the {@link HttpServletResponse} object that
     *             contains the response the servlet returns
     *             to the client
     * @throws IOException      if an input or output error occurs
     *                          while the servlet is handling the
     *                          DELETE request
     * @throws ServletException if the request for the
     *                          DELETE cannot be handled
     */
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info(Constants.DO_DELETE);
        int paramIdBook = Integer.parseInt(req.getParameter("id"));
        IBookDAO bookDAO = InfoFactory.getClassFromFactory();
        try {
            bookDAO.deleteBook(paramIdBook);
        } catch (DAOException e) {
            LOGGER.error(e.getStackTrace());
            forwardError(e.getMessage(), Constants.ERROR_PAGE, req, resp);
        }
    }

}
