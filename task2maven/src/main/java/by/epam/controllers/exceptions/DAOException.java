package by.epam.controllers.exceptions;


import org.apache.log4j.Logger;

public class DAOException extends Exception {
    private static final Logger LOGGER = Logger.getLogger(DAOException.class);

    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public DAOException(String message) {
        super(message);
        LOGGER.error(message);
    }

    /**
     * Constructs a new exception with the specified detail message and
     * cause.
     *
     * @param message the detail message (which is saved for later retrieval
     *                by the {@link #getMessage()} method).
     * @param cause   the cause (which is saved for later retrieval by the
     *                {@link #getCause()} method).  (A <tt>null</tt> value is
     *                permitted, and indicates that the cause is nonexistent or
     *                unknown.)
     * @since 1.4
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
        LOGGER.error(message,cause);
    }

    /**
     * Constructs a new exception with the specified cause and a detail
     * message.
     * @param cause the cause (which is saved for later retrieval by the
     *              {@link #getCause()} method).  (A <tt>null</tt> value is
     *              permitted, and indicates that the cause is nonexistent or
     *              unknown.)
     * @since 1.4
     */
    public DAOException(Throwable cause) {
        super(cause);
        LOGGER.error(cause);
    }
}
