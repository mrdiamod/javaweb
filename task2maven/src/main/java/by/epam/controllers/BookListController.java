package by.epam.controllers;

import by.epam.controller.utils.Constants;
import by.epam.controllers.exceptions.DAOException;
import by.epam.interfaces.IBookDAO;
import by.epam.model.beans.Book;
import by.epam.model.factory.InfoFactory;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Servlet for Book DAO list.
 *
 * method doGet print information Books in page
 *
 * <p>
 * extends abstract base controller
 */

public class BookListController extends AbstractBaseController {
    private static final Logger LOGGER = Logger.getLogger(BookListController.class);
    private static AtomicBoolean dispasableFlagForError = new AtomicBoolean(true);

    /**
     * Override 'doGet' method. Set Book from database to Books.jsp.
     *
     * @param request  request from browser
     * @param response response from servlet
     * @throws ServletException Servlet exception
     * @throws IOException      IO exception
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info(Constants.DO_GET);
        String isAccessInPage = request.getParameter("access");
        String messageForError = request.getParameter(Constants.PARAM_FLAG_ERROR);
        List<Book> books = new ArrayList<>();
        IBookDAO bookDAO = InfoFactory.getClassFromFactory();
        if (dispasableFlagForError.get() && messageForError != null && messageForError.equals("Don't click Button")) {
            dispasableFlagForError.compareAndSet(true, false);
            throw new UnavailableException(Constants.ERROR_OUUPS, 10);
        }
        try {
            books = bookDAO.getAllBooks();
        } catch (DAOException e) {
            LOGGER.error(e.getStackTrace());
            forwardError(e.getMessage(), Constants.ERROR_PAGE, request, response);
        }
        request.setAttribute(Constants.KEY_BOOKS, books);
        if((request.isUserInRole("admin") || request.isUserInRole("manager")) &&
                (isAccessInPage!=null && isAccessInPage.equals("true"))){
            forward(Constants.OPERATION_LIST_BOOKS_PAGE, request, response);

        }
        forward(Constants.START_PAGE, request, response);
    }

}
