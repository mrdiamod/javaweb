package by.epam.interfaces;

import by.epam.controllers.exceptions.DAOException;
import by.epam.model.beans.Book;

import java.util.List;

/**
 * * interface Book DAO.
 * <p>
 * methods:
 * public List<Book> getBooks() return list books.
 */
public interface IBookDAO {

    /**
     * public method with Get all books from epamlib.
     *
     * @return List<Book> List of books
     * @throws DAOException SQL exception
     */
    List<Book> getAllBooks() throws DAOException;

    /**
     * public void method when add book in epamlib.
     * param: this is fields Book description
     * @param paramBookName
     * @param paramBookAuthor
     * @param paramBookDescript
     * @param paramBookDate
     * @param paramBookPrice
     * @return not return, operation add book in List of books
     * @throws DAOException SQL exception
     */
    void addBook(String paramBookName, String paramBookAuthor,
                  String paramBookDescript, String paramBookDate, String paramBookPrice) throws DAOException;

    /**
     * public void method when edit book from epamlib.
     * param: this is fields Book description
     * @param paramBookId
     * @param paramBookName
     * @param paramBookAuthor
     * @param paramBookDescript
     * @param paramBookDate
     * @param paramBookPrice
     * @return not return, operation edit book in List of books
     * @throws DAOException SQL exception
     */
    void editBook(String paramBookId, String paramBookName, String paramBookAuthor,
                  String paramBookDescript, String paramBookDate, String paramBookPrice) throws DAOException;

    /**
     * public method with Get book from epamlib.
     *
     * @param id seacrh book by id in DB
     * @return Book
     * @throws DAOException SQL exception
     */
    Book getBook(int id) throws DAOException;

    /**
     * public method with delete book from epamlib.
     *
     * @param id delete book by id in DB
     * @throws DAOException SQL exception
     */
    void deleteBook(int id) throws DAOException;
}
