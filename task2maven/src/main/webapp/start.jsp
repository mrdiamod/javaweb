<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 11.12.2019
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
    <script src="js/xhr.js"></script>
    <title>Title</title>
</head>

<body>

    <c:set var="isAccess" scope="session" value="${pageContext.request.isUserInRole('admin')}"/>
    <header>
        <div>
        <c:if test="${isAccess}">
            <td>Menu option</td>
            <td><a name="adminList"  href='<c:url value='/start?access=${isAccess}' />'>ListBooks</a></td>
            <td><a href="<c:url value='/operation/addBook.jsp'/>">Add Book</a></td>
        </c:if>
            <td><a href="<c:url value='/logout'/>">Logout</a></td>
        </div>
    </header>
    <c:out value="${isAccess}"/>
    <table class="table">
        <c:forEach items="${books}" var="book">
            <tr>
                <c:if test="${isAccess}">
                    <td><input type="checkbox" name="bookId" value="${book.id}"></td>
                </c:if>
                <td>${book.id}</td>
                <td>${book.name}</td>
                <c:if test="${isAccess}">
                    <td><a name="idBook"  value="${book.id}" href='<c:url value='/editBook?id=${book.id}' />'>Edit</a></td>
                    <td><a name="idBook" href="JavaScript:deleteBook(${book.id})" >Delete</a></td>
                </c:if>
            </tr>
        </c:forEach>
    </table>
    <a href="<c:url value='/operation/addBook.jsp'/>">Add Book</a>
    <form name="registerForm" method="GET" action="<c:url value='/start'/>">
        <input type="submit" name="flagError" value="Don't click Button">
    </form>
    <a href="<c:url value='secure/additionalInfoApp.jsp'/>">Info</a>
</body>
</html>
