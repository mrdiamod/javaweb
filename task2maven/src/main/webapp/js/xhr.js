const requestDeleteURL = '/operation/deleteBook?id='
const pathName = '/task2mav'
const host = location.protocol + '//' + location.host
const pathNameStart = '/start'


/*    <a href="JavaScript:upLoadFile(${task.id})">Upload</a>*/

function deleteBook(idBook) {
    const xhr = new XMLHttpRequest()
    xhr.open('DELETE', host + pathName + requestDeleteURL + idBook, false)
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            reset();
            alert("Staff: " + idBook + " successfully delete");
        }
    };
    xhr.send()
}

function reset() {
    const xhr = new XMLHttpRequest()
    xhr.open('GET', host + pathName + pathNameStart, false)
    xhr.send()
}