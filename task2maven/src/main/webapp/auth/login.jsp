<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Log In</title>
</head>
<body>
<div>
    <h2>Please log in </h2>
    <form action="j_security_check" method="post">
        <div>
            <input type="text" name="j_username" placeholder="Username">
            <input type="password" name="j_password" placeholder="Password">
            <input type="submit" value="Login">
        </div>
    </form>
</div>
</body>
</html>