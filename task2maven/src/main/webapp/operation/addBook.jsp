<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 12.01.2020
  Time: 00:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Title</title>
    </head>

    <body>
        Hello
        <form name="addThisBook" method="POST" action="<c:url value='/addBook'/>">
            <input type="hidden" name="eventClickAddBook" value="add">
                <label>Name</label>
                <input type="text"  name="addBookName"  value="" placeholder="What is name book?">
                <label>Autor</label>
                <input type="text"  name="addBookAuthor"  value="" placeholder="What is author?">
                <label>Description</label>
                <input type="text"  name="addBookDescription"  value="" placeholder="Description book">
                <label>The year of publishing</label>
                <input type="text"  name="addBookDate" id="date" placeholder="1953">
                <label>Price Book</label>
                <input type="text"  class="" name="addBookPrice" placeholder="200">
            <input type="submit" class="" value="Add new Book">
        </form>

        <a href="<c:url value='/start'/>">Back in page</a>
    </body>
</html>
