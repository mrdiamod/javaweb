<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 13.01.2020
  Time: 1:21
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Edit Book</title>
</head>

<body>
    Hello
    <form name="addThisBook" method="POST" action="<c:url value='/editBook'/>">
        <input type="hidden" name="editBookId" value="${editBook.id}">
        <label>Name</label>
        <input type="text" name="editBookName" placeholder="What is name book?" value="${editBook.name}">
        <label>Autor</label>
        <input type="text" name="editBookAuthor" placeholder="What is author?" value="${editBook.author}">
        <label>Description</label>
        <input type="text" name="editBookDescription" placeholder="Description book" value="${editBook.description}">
        <label>The year of publishing</label>
        <input type="text" name="editBookDate" id="date" placeholder="1953" value="${editBook.year}">
        <label>Price Book</label>
        <input type="text" class="" name="editBookPrice" placeholder="200" value="${editBook.price}">
        <input type="submit" class="" value="Confirm Changes Book">
    </form>
    <a href="<c:url value='/start'/>">Back in page</a>
</body>
</html>
