<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 11.12.2019
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
    <title></title>
</head>

<body>

<table class="table">
    <c:forEach items="${books}" var="book">
        <tr>
            <td><input type="checkbox" name="bookId" value="${book.id}"></td>
            <td>${book.id}</td>
            <td>${book.name}</td>
            <td><a name="idBook" value="${book.id}" href='<c:url value='/editBook?id=${book.id}' />'>Edit</a></td>
            <td><a name="idBook"  value="${book.id}" href='<c:url value='/operation/deleteBook?id=${book.id}' />'>Delete</a></td>
        </tr>
    </c:forEach>
</table>
<a href="<c:url value='/operation/addBook.jsp'/>">Add Book</a>

<form name="registerForm" method="GET" action="<c:url value='/start'/>">
    <%--input type="text" name=<%="flagError"%> value=""--%>
    <input type="submit" name="flagError" value="Don't click Button">
</form>
<a href="<c:url value='/secure/additionalInfoApp.jsp'/>">Info</a>
</body>
</html>
