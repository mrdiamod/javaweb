<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="m" uri="/WEB-INF/mytags.tld" %>

<%--
  Created by IntelliJ IDEA.
  User: Admin
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Info App</title>
    </head>

    <body>
        <div>
            Current Date and Time is:
            <m:topass>
              ${mapServlet} ::  ${nameServlet}
                </br>
            </m:topass>
            <%--<% int a = Calendar.getInstance().getTime().getSeconds();%>
            Fibonacci by :  ${m:fibonacciBySeconds(<%= a%>)}--%>
            </br>
            Fibonacci by number 8:  ${m:calculateFibonacci(8)}
        </div>
        <a href="<c:url value='/start'/>">Back</a>
    </body>
</html>
