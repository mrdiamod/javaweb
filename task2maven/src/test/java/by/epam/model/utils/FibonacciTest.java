package by.epam.model.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FibonacciTest {

    @Test
    public void calculatePosition() {
        assertEquals(0,Fibonacci.calculatePosition(0));
        assertEquals(1,Fibonacci.calculatePosition(-1));
        assertEquals(1,Fibonacci.calculatePosition(-2));
        assertEquals(3,Fibonacci.calculatePosition(4));
        assertEquals(5,Fibonacci.calculatePosition(5));
        assertEquals(8,Fibonacci.calculatePosition(6));
        assertEquals(13,Fibonacci.calculatePosition(7));
        assertEquals(6765,Fibonacci.calculatePosition(20));
        assertEquals(701408733,Fibonacci.calculatePosition(44));
        //limit to 44, int value
    }
}
