package by.epam.filters;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class XSSRequestWrapperTest {
    private static HttpServletRequest servletRequest;
    private static String parameter;

    @BeforeClass
    public static void beforeClass() {
        servletRequest = mock(HttpServletRequest.class);
        parameter = "something";
    }

    @Test
    // if return null
    public void getParameterValues() {
        when(servletRequest.getParameterValues(parameter)).thenReturn(null);
        String title = new XSSRequestWrapper(servletRequest).getParameter(parameter);
        assertNull(title);
    }

    @Test
    public void getParameter() {
        final Map<String, String> testData = new HashMap<>();
        testData.put("<script>", "&lt;&gt;");
        testData.put("< script>", "&lt;&gt;");
        testData.put("<script  >", "&lt;&gt;");
        testData.put("<script>some script</script>", "&lt;&gt;some&lt;/&gt;");
        testData.put("test<script>some script</script>", "test&lt;&gt;some&lt;/&gt;");
        testData.put("</", "&lt;/");
        testData.put("<tag>data</tag>", "&lt;tag&gt;data&lt;/tag&gt;");
        testData.put("<script  ><tag>data</tag> < script>", "&lt;&gt;&lt;tag&gt;data&lt;/tag&gt;&lt;&gt;");
        testData.forEach((value, expectedValue) -> {
            when(servletRequest.getParameter(parameter)).thenReturn(value);
            String title = new XSSRequestWrapper(servletRequest).getParameter(parameter);
            assertEquals(expectedValue, title);
        });
    }
}
